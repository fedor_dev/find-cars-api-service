module.exports = {
	db: {
    	host : '127.0.0.1',
    	user : 'postgres',
    	password : '123',
    	database : 'postgres',
    },
    mongoCache: 'mongodb://127.0.0.1:27017',

    currencyTtl: 3 * 60 * 60,
    currencies: ['USD', 'EUR'],
    bankApi: 'https://www.cbr-xml-daily.ru/daily_json.js',

    brands: ['audi', 'bmw', 'lexus', 'jaguar', 'opel', 'porsche'],
    colors: ['red', 'black', 'white', 'silver', 'blue'],
};
