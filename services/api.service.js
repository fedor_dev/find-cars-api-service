"use strict";

const ApiGateway = require("moleculer-web");

module.exports = {
	name: "api",
	mixins: [ApiGateway],

	settings: {
		port: process.env.PORT || 3000,

		routes: [{
			path: "/",
            authorization: false,
			whitelist: [
				"currencies.*",
				"generator.*",
				"cars.*",
			],
			aliases: {
				"GET generator/read/:id": "generator.read",
				"GET generator/delete/:id": "generator.delete",
				"GET cars/find/:mark": "cars.find",
			},
		}],

		// Serve assets from "public" folder
		assets: {
			folder: "public"
		}
	}
};
