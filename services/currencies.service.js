"use strict";

const redis = require('redis');
const axios = require('axios');
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
const redisCli = redis.createClient();
const config = require('../config.js');

// microservice for currency rates
module.exports = {
	name: "currencies",

	/**
	 * Service settings
	 */
	settings: {
		ttl: config.ttl, // refresh currency rates on this interval, in sec
	},

	/**
	 * Service dependencies
	 */
	dependencies: [],	

	/**
	 * Actions
	 */
	actions: {
		async list() {
			const res = {};

			for (let item of config.currencies) {
				const val = await redisCli.getAsync(item);

				if (!val) { // reload currency rates and call again
					console.log('expired, need to reload rates');
					return this.fetchRates()
						.then(() => this.actions.list());
				}

				res[item] = val;
			}
			return res;
		},

		update(ctx) {
			return this.fetchRates()
				.then(() => {
					ctx.emit('rates.updated', true);
					return { status: 'Update in progress...' };
				});
		},

		/**
		 * @param {String} name - currency name
		 */
		rate: {
			params: {
				name: "string"
			},
			async handler(ctx) {
				if (!config.currencies.includes(ctx.params.name)) {
					return { error: 'unknown currency '};
				}

				const val = await this.getRate(ctx.params.name);
				return { [ctx.params.name]: val };
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		async getRate (name) {
			let res = await redisCli.getAsync(name);
			if (!res) {
				await this.fetchRates();
				res = redisCli.getAsync(name);
			}
			return res;
		},

		fetchRates () {
			return axios.get(config.bankApi)
				.then((req) => {
					for (let currency in req.data.Valute) {
						if (config.currencies.includes(currency)) {
							const val = req.data.Valute[currency].Value;
							redisCli.set(currency, val, 'EX', config.currencyTtl);
						}
					}
				})
				.catch((err) => {
					console.log(err);
				});
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.fetchRates();
	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};