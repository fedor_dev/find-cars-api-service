"use strict";

const redis = require('redis');
const axios = require('axios');
const bluebird = require('bluebird');
const config = require('../config.js');

const db = require('knex')({
  client: 'pg',
  connection: config.db
});

bluebird.promisifyAll(redis.RedisClient.prototype);
const redisCli = redis.createClient();

// microservice for currency rates
module.exports = {
	name: "generator",

	/**
	 * Service settings
	 */
	settings: {
		brands: config.brands,
		colors: config.colors,
	},

	/**
	 * Service dependencies
	 */
	dependencies: [],	

	/**
	 * Actions
	 */
	actions: {
		create: {
			params: {
				mark: "string",
				color: "string",
				price: "string",
			},
			async handler(ctx) {
				if (!this.settings.brands.includes(ctx.params.mark)) {
					return { error: 'unknown mark' };
				}

				if (!this.settings.colors.includes(ctx.params.color)) {
					return { error: 'unknown color' };
				}

				const price = parseInt(ctx.params.price, 10);
				if (price < 1) {
					return { error: 'price cant be negative or zero' };
				}

				await this.addCar({
					price,
					mark: ctx.params.mark,
					color: ctx.params.color,
				});
				ctx.emit('cars.updated', true);
				return { status: 'added' };
			}
		},

		update: {
			params: {
				id: "string",
			},
			async handler(ctx) {
				const id = parseInt(ctx.params.id, 10);
				const args = {};

				if (!id || id < 1) {
					return { error: 'bad id' };
				}

				if (ctx.params.mark) {
					if (!this.settings.brands.includes(ctx.params.mark)) {
						return { error: 'unknown mark' };
					}
					args.mark = ctx.params.mark;
				}

				if (ctx.params.color) {
					if (!this.settings.colors.includes(ctx.params.color)) {
						return { error: 'unknown color' };
					}
					args.color = ctx.params.color;
				}

				const price = parseInt(ctx.params.price, 10);
				if (ctx.params.price) {
					if (price < 1) {
						return { error: 'price cant be negative or zero' };
					}
					args.price = price;
				}

				await this.editCar(id, args);
				ctx.emit('cars.updated', true);
				return { status: 'updated' };
			}
		},

		generate: {
			async handler(ctx) {
				console.log('called aue', ctx.params);
				await this.generateCars(ctx.params.count || 10);
				ctx.emit('cars.updated', true);
				return true;
			}
		},

		read: {
			params: {
				"id": "string",
			},
			async handler(ctx) {
				const id = parseInt(ctx.params.id, 10);

				if (!id || id < 1) {
					return { error: 'bad id' };
				}
				const data = await this.showCar(id);
				return data[0] || { error: 'not found' };
			}
		},

		delete: {
			params: {
				"id": "string",
			},
			async handler(ctx) {
				const id = parseInt(ctx.params.id, 10);

				if (!id || id < 1) {
					return { error: 'bad id' };
				}

				await this.deleteCar(id);
				ctx.emit('cars.updated', true);
				return { status: 'deleted' };
			}
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		addCar(args) {
			return db('cars').insert(args).then((res) => res);
		},
		showCar(id) {
			return db.select('id', 'mark', 'color', 'price').from('cars').where('id', id);
		},
		editCar(id, args) {
			return db('cars').where('id', id).update(args);
		},
		deleteCar(id) {
			return db('cars').where('id', id).del();
		},
		async generateCars(count) {
			for (let i = 0; i < count; i = i+1) {
				const res = {};
				res.price = Math.floor(Math.random() * (950000 - 200000) + 200000);
				res.mark = this.settings.brands[Math.floor(Math.random() * this.settings.brands.length)];
				res.color = this.settings.colors[Math.floor(Math.random() * this.settings.colors.length)];
				await this.addCar(res).then(() => console.log('added', res));
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	async created() {
		await db.schema.dropTable('cars');
		await db.schema.createTable('cars', function (t) {
  			t.increments();
  			t.string('mark');
  			t.string('color');
  			t.integer('price')
				.unsigned()
				.notNull();
		});

		this.actions.generate({ count: 20 });
	},

	/**
	 * Service started lifecycle event handler
	 */
	started(ctx) {
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};