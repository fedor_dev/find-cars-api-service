"use strict";

const _ = require('lodash');
const redis = require('redis');
const axios = require('axios');
const bluebird = require('bluebird');
const mongoClient = require('mongodb').MongoClient;

const config = require('../config.js');

const db = require('knex')({
  client: 'pg',
  connection: config.db,
});

bluebird.promisifyAll(redis.RedisClient.prototype);
const redisCli = redis.createClient();

let mongodb = {};
// microservice for currency rates
module.exports = {
	name: "cars",

	/**
	 * Service settings
	 */
	settings: {
		brands: config.brands,
		colors: config.colors,
		currencies: config.currencies,
	},

	/**
	 * Service dependencies
	 */
	dependencies: [],	

	/**
	 * Actions
	 */
	actions: {
		async find (ctx) {
			const args = {};
			let limit = 20;
			let offset = 0;
			let cur = 'USD';

			if (ctx.params.limit) {
				limit = parseInt(ctx.params.limit, 10) || 20;
			}

			if (ctx.params.offset) {
				offset = parseInt(ctx.params.offset, 10) || 20;
			}

			if (ctx.params.cur) {
				const currency = _.toUpper(ctx.params.cur);
				if (!config.currencies.includes(currency)) {
					return { error: 'unkown currency' };
				}
				cur = currency;
			}

			if (ctx.params.mark) {
				if (!this.settings.brands.includes(ctx.params.mark)) {
					return { error: 'unknown mark' };
				}
				args.mark = ctx.params.mark;
			}

			if (ctx.params.color) {
				if (!this.settings.colors.includes(ctx.params.color)) {
					return { error: 'unknown color' };
				}
				args.color = ctx.params.color;
			}

			let rate = await redisCli.getAsync(cur);
			if (!rate) {
				await ctx.call('currencies.update');
				rate = await redisCli.getAsync(cur);
			}

			const total = await this.count(args, cur);
			const data = await this.list(args, limit, offset, cur);
			return { page: offset/limit + 1, total, cars: data };
		},

		async reload (ctx) {
			console.log('reload action called!');
			const params = [];

			// check currencies
			for (let item of config.currencies) {
				const val = await redisCli.getAsync(item);
				if (!val) {
					await ctx.call('currencies.update');
					return true;
				}
				params.push({ name: item, rate: val });
			}

			await this.reloadCache(params);
			return { status: 'cache reloaded' };
		},

		async generate (ctx) {
			const data = await ctx.call('generator.generate', 1);
			return { status: 'generating new data...', data };
		},
	},

	/**
	 * Events
	 */
	events: {
		'cars.updated' (data) {
			console.log('EVENT called cars.updated');
			this.actions.reload();
		},
		'rates.updated' (data) {
			console.log('EVENT called rates.updated');
			this.actions.reload();
		}
	},

	/**
	 * Methods
	 */
	methods: {
		list(params, limit, offset, cur) {
			return mongodb.db('test').collection(`cars_${cur}`).find(params).limit(limit).toArray();
		},

		count(params, cur) {
			return mongodb.db('test').collection(`cars_${cur}`).countDocuments(params);
		},

		async reloadCache (currencies) {
			console.log(currencies);
			for (let item of currencies) {
				const rate = parseFloat(item.rate);
				await mongodb.db('test').collection(`cars_${item.name}`).deleteMany({ price: { $ne: 0 } });
				const data = await db.select('*').from('cars');
				data.forEach((car) => {
					const newPrice = parseFloat(car.price) / rate;
					car.price = newPrice.toFixed(2);
					car.cur = item.name;
				});
				await mongodb.db('test').collection(`cars_${item.name}`).insertMany(data);
				console.log('done for ', item.name);
			}
			return true;
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		console.log('connecting to MongoDB');
    	mongodb = await mongoClient.connect(config.mongoCache)
    		.catch((err) => { throw new Error(err); });
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {
		console.log('disconnecting from MongoDB');
		mongodb.close();
	}
};