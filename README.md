Sample API for cars marketplace. Prices in different currencies, caching with MongoDB.
Node.js project powered by Moleculer framework

[![Moleculer](https://img.shields.io/badge/Powered%20by-Moleculer-green.svg?colorB=0e83cd)](https://moleculer.services)

# demo

## Build Setup

``` bash
# Install dependencies
npm install

# Start developing with REPL
npm run dev

# Start production
npm start

# Run ESLint
npm run lint
```

